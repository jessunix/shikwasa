const CONFIG = {
  fixed: {
    value: false,
    position: null,
  },
  container: document.querySelector('body'),
  themeColor: '#00869B',
  autoPlay: false,
  muted: false,
  preload: 'metadata',
  speedOptions: [0.5, 0.75, 1.25, 1.5],
  audio: null,
}

export default CONFIG
